# Create Nextjs App CLI

Easily spin up a nextjs app with one command.

```sh
$ bunx create-nextjs-app

# or npx create-nextjs-app
```

### Tech list

- Tailwind
- Nextjs
- Turbo Repo
- Bun
- Husky
- Web3Auth (optional) 
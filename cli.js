#!/usr/bin/env node

import chalk from 'chalk'
import path from 'path'
import fs from 'fs'
import { input } from '@inquirer/prompts'
import { Command } from 'commander'
import select from '@inquirer/select'
import {execa, execaCommand} from 'execa'
import ora from 'ora'
import childProcess from 'child_process'

const log = console.log
const program = new Command()
const green = chalk.green

const isPnpmInstalled = () => {
  try {
    childProcess.execSync('pnpm --version');
    return true;
  } catch {
    return false; 
  }
}

const isBunInstalled = () => {
  try {
    childProcess.execSync('bun --version')
    return true;
  } catch(err) {
    return false; 
  }
}

async function main() {
  const spinner = ora({
    text: 'Creating codebase'
  })
  try {
    const kebabRegez = /^([a-z]+)(-[a-z0-9]+)*$/

    program
      .name('Create Lens App')
      .description('Create a new social app with a single command.')
      .option('-t, --type <type of app>', 'Set the app type as basic or PWA')
  
    program.parse(process.argv)
  
    const options = program.opts()
    const args = program.args
    let type = options.type
    let appName = args[0]
  
    if (!appName || !kebabRegez.test(args[0])) {
      appName = await input({
        message: 'Enter your app name',
        default: 'nextjs-app',
        validate: d => {
         if(!kebabRegez.test(d)) {
          return 'please enter your app name in the format of my-app-name'
         }
         return true
        }
      })
    }
  
    // if (
    //   !options.type ||
    //   (
    //     options.type !== 'basic' &&
    //     options.type !== 'pwa' &&
    //     options.type !== 'opinionated'
    //   )
    // ) {
    //   type = await select({
    //     message: 'Select an app type',
    //     choices: [
    //       {
    //         name: 'basic',
    //         value: 'basic',
    //         description: 'Basic responsive Lens web app configured with WalletConnect and ShadCN UI',
    //       },
    //       {
    //         name: 'opinionated',
    //         value: 'opinionated',
    //         description: 'More opinionated, responsive Lens web app configured with WalletConnect and ShadCN UI',
    //       },
    //       {
    //         name: 'PWA (Progressive Web App)',
    //         value: 'pwa',
    //         description: 'PWA Lens app configured with WalletConnect and ShadCN UI',
    //       }
    //     ]
    //   })
    // }
  
    let repoUrl = 'https://gitlab.com/gwave_mobile_public/nextjs-boilerplate'

    // if (type === 'opinionated') {
    //   repoUrl = 'https://github.com/dabit3/lens-shadcn'
    // }

    // if (type === 'pwa') {
    //   repoUrl = 'https://github.com/dabit3/lens-pwa'
    // }

    log(`\nInitializing project with template: ${chalk.cyan(repoUrl)} \n`)

    if (!isBunInstalled()) {
log(`
Installing dependencies:
`)
    }

    spinner.start()
    await execa('git', ['clone', repoUrl, appName])

    let packageJson = fs.readFileSync(`${appName}/package.json`, 'utf8')
    const packageObj = JSON.parse(packageJson)
    packageObj.name = appName
    packageJson = JSON.stringify(packageObj, null, 2)
    fs.writeFileSync(`${appName}/package.json`, packageJson)

    process.chdir(path.join(process.cwd(), appName))
    spinner.text = ''
    let startCommand = ''

    if (isBunInstalled()) {
      spinner.text = 'Installing dependencies'
      await execaCommand('bun install').pipeStdout(process.stdout)
      spinner.text = ''
      startCommand = 'bun dev'
      console.log('\n')
    } else if (isPnpmInstalled()) {
      await execaCommand('pnpm').pipeStdout(process.stdout)
      startCommand = 'pnpm dev'
    } else {
      log('Error: please install bun or pnpm first. https://bun.sh/docs/installation or https://pnpm.io/installation')
      spinner.stop()
    }
    spinner.stop() 
    log(`${green.bold('Success!')} Created ${appName} at ${process.cwd()} \n`)
    log(`To get started, change into the new directory and run ${chalk.cyan(startCommand)}`)
  } catch (err) {
    log('\n')
    if (err.exitCode == 128) {
      log('Error: directory already exists.')
    }
    spinner.stop()
  }
}
main()